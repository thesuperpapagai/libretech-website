//Ads process
function ads_generator(ga_id, ga_slot_id, slot_width, slot_height)
{
	return ga_slot_id > 0 && show_ga?'<ins class="adsbygoogle" style="display:inline-block;width:'+slot_width+'px;height:'+slot_height+'px" data-ad-client="'+ga_id+'" data-ad-slot="'+ga_slot_id+'"></ins>':'<iframe width="'+slot_width+'" scrolling="no" height="'+slot_height+'" frameborder="0" allowtransparency="true" hspace="0" vspace="0" marginheight="0" marginwidth="0" src="'+base_url+'assets/ads.php"></iframe>';
}
function ads_desktop_size(element)
{
	var ga_id = 'ca-pub-0359'+'638389509980';
	var ga_slot_id 	= $(element).attr('slot-id');
	var slot_width 	= $(element).width();
	var slot_height = $(element).height();
	var slot_width_set 	= parseInt($(element).attr('slot-width'), 10);
	var slot_height_set = parseInt($(element).attr('slot-height'), 10);


	if(slot_width >= 980)
	{
		slot_width = slot_width_set > 0?slot_width_set:980;
		slot_height = slot_height_set > 0?slot_height:120;
	}
	else if(slot_width >= 930)
	{
		slot_width = slot_width_set > 0?slot_width_set:930;
		slot_height = slot_height_set > 0?slot_height:180;
	}
	else if(slot_width >= 750)
	{
		slot_width = slot_width_set > 0?slot_width_set:750;
		slot_height = slot_height_set > 0?slot_height:200;
	}
	else if(slot_width >= 728)
	{
		slot_width = slot_width_set > 0?slot_width_set:728;
		slot_height = slot_height_set > 0?slot_height:90;
	}
	else if(slot_width >= 336)
	{
		slot_width = slot_width_set > 0?slot_width_set:336;
		slot_height = slot_height_set > 0?slot_height:280;
	}
	else if(slot_width >= 300)
	{
		slot_width = slot_width_set > 0?slot_width_set:300;
		slot_height = slot_height_set > 0?slot_height:250;
	}
	else if(slot_width >= 250)
	{
		slot_width = slot_width_set > 0?slot_width_set:250;
		slot_height = slot_height_set > 0?slot_height:250;
	}
	else if(slot_width >= 200)
	{
		slot_width = slot_width_set > 0?slot_width_set:200;
		slot_height = slot_height_set > 0?slot_height:200;
	}

	return ads_generator(ga_id, ga_slot_id, slot_width, slot_height);
}
function ads_show()
{
	if(show_ga) $('head').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>');
	$('.mmo').each(function()
	{
		ads_code = ads_desktop_size(this);
		$(this).html(ads_code);
		if(show_ga) (adsbygoogle = window.adsbygoogle || []).push({});
	});
}
ads_show();
