function getCookie(a) {
    var b = document.cookie.match("(^|;) ?" + a + "=([^;]*)(;|$)");
    return b ? b[2] : null
}

function setCookie(a, b, c) {
    var d = new Date;
    d.setTime(d.getTime() + 864e5 * c), document.cookie = a + "=" + b + ";path=/;expires=" + d.toGMTString()
}

function wopen(a, b, c, d) {
    d = d || "", b = b || 640, c = c || 350;
    var e = (window.screenLeft ? window.screenLeft : window.screenX) + ww / 2 - b / 2,
        f = (window.screenTop ? window.screenTop : window.screenY) + wh / 2 - c / 2 - 100;
    return f <= 0 && (f = 20), window.open(a, d, "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + b + ",height=" + c + ",top=" + f + ",left=" + e), !1
}

function resized(a) {
    ww = $(window).innerWidth(), wh = $(window).innerHeight(), setCookie("client_width", ww, 365);
    var b = $.colorbox.settings;
    $.colorbox.resize({
        width: ww > b.maxWidth ? b.maxWidth : b.width,
        height: wh > b.maxHeight ? b.maxHeight : b.height
    }), a || ($(".autosize").not(".autosized").addClass("autosized").autosize({
        placeholder: 0,
        callback: function() {
            setTimeout(function() {
                resized(!0)
            }, 150)
        }
    }), $(".autosize").trigger("autosize.resize"), (is_ie || is_safari) && $("[placeholder]").not(".polyfilled").addClass("polyfilled").focus(function() {
        var a = $(this);
        a.hasClass("placeholder") && a.caret(0)
    }).click(function() {
        var a = $(this);
        a.hasClass("placeholder") && a.caret(0)
    }).keydown(function() {
        var a = $(this);
        a.hasClass("placeholder") && (a.val(""), a.removeClass("placeholder"))
    }).blur(function() {
        var a = $(this);
        "" != a.val() && a.val() != a.attr("placeholder") || a.addClass("placeholder").val(a.attr("placeholder"))
    }).blur().parents("form").not(".polyfilled").addClass("polyfilled").submit(function() {
        $(this).find("[placeholder]").each(function() {
            $(this).val() == $(this).attr("placeholder") && $(this).val("")
        })
    }))
}

function flexVideoGrid(a, b, c) {
    $(".flex_grid.video").flexImages({
        rowHeight: a,
        maxRows: b,
        truncate: c
    }).find(".media").hover(function() {
        hover_media = $(this), hover_media_to = setTimeout(function() {
            hover_media.prepend('<video style="display:none" onplaying="$(this).addClass(\'playing\').show();" autoplay muted loop><source type="video/mp4" src="' + hover_media.data("video") + '"></video>')
        }, 300)
    }, function() {
        $(this).find("video").remove(), clearTimeout(hover_media_to)
    })
}

function show_message(a, b, c, d) {
    $(".message_box").remove();
    var e = $('<div class="message_box" onclick="$(this).remove();"><span style="opacity:.8;float:right;margin-right:8px;cursor:pointer;font-size:20px;line-height:1">×</span>' + a + "</div>");
    b && e.addClass(b), d ? e.css("position", "relative").prependTo("body") : e.appendTo("body"), e.delay(c || 4500).slideUp()
}

function show_support_overlay() {
    var a = $("#support_overlay"),
        b = $("#media_container > img");
    a.length && b.width() > 370 && b.height() > 180 ? ($("#img_click_overlay").hide(), a.show(), b.height() > 470 ? (! function(a, b, c) {
        var d, e = a.getElementsByTagName(b)[0];
        a.getElementById(c) || (d = a.createElement(b), d.id = c, d.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1818229235120698", e.parentNode.insertBefore(d, e))
    }(document, "script", "facebook-jssdk"), function() {
        var a = document.createElement("script");
        a.type = "text/javascript", a.async = !0, a.src = "https://apis.google.com/js/plusone.js";
        var b = document.getElementsByTagName("script")[0];
        b.parentNode.insertBefore(a, b)
    }(), ! function(a, b, c) {
        var d, e = a.getElementsByTagName(b)[0];
        a.getElementById(c) || (d = a.createElement(b), d.id = c, d.src = "https://platform.twitter.com/widgets.js", e.parentNode.insertBefore(d, e))
    }(document, "script", "twitter-wjs")) : $(".hide_on_small_img").hide(), $("#media_show .overlay").fadeIn()) : $("#media_show .overlay").fadeOut()
}
try {
    jQuery = django.jQuery, $ = jQuery, LANG = "en", I18N_DELETE = "Really delete?"
} catch (a) {}
var ua = navigator.userAgent.toLowerCase(),
    is_ie = ~ua.indexOf("msie ") || ~ua.indexOf("trident/"),
    is_safari = ~ua.indexOf("safari") && !~ua.indexOf("chrome"),
    ac_xhr, hover_media, hover_media_to, human_visitor;
! function(a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("undefined" != typeof jQuery ? jQuery : window.Zepto)
}(function(a) {
    "use strict";

    function b(b) {
        var c = b.data;
        b.isDefaultPrevented() || (b.preventDefault(), a(b.target).ajaxSubmit(c))
    }

    function c(b) {
        var c = b.target,
            d = a(c);
        if (!d.is("[type=submit],[type=image]")) {
            var e = d.closest("[type=submit]");
            if (0 === e.length) return;
            c = e[0]
        }
        var f = this;
        if (f.clk = c, "image" == c.type)
            if (void 0 !== b.offsetX) f.clk_x = b.offsetX, f.clk_y = b.offsetY;
            else if ("function" == typeof a.fn.offset) {
            var g = d.offset();
            f.clk_x = b.pageX - g.left, f.clk_y = b.pageY - g.top
        } else f.clk_x = b.pageX - c.offsetLeft, f.clk_y = b.pageY - c.offsetTop;
        setTimeout(function() {
            f.clk = f.clk_x = f.clk_y = null
        }, 100)
    }

    function d() {
        if (a.fn.ajaxSubmit.debug) {
            var b = "[jquery.form] " + Array.prototype.join.call(arguments, "");
            window.console && window.console.log ? window.console.log(b) : window.opera && window.opera.postError && window.opera.postError(b)
        }
    }
    var e = {};
    e.fileapi = void 0 !== a("<input type='file'/>").get(0).files, e.formdata = void 0 !== window.FormData;
    var f = !!a.fn.prop;
    a.fn.attr2 = function() {
        if (!f) return this.attr.apply(this, arguments);
        var a = this.prop.apply(this, arguments);
        return a && a.jquery || "string" == typeof a ? a : this.attr.apply(this, arguments)
    }, a.fn.ajaxSubmit = function(b) {
        function c(c) {
            var d, e, f = a.param(c, b.traditional).split("&"),
                g = f.length,
                h = [];
            for (d = 0; g > d; d++) f[d] = f[d].replace(/\+/g, " "), e = f[d].split("="), h.push([decodeURIComponent(e[0]), decodeURIComponent(e[1])]);
            return h
        }

        function g(d) {
            for (var e = new FormData, f = 0; f < d.length; f++) e.append(d[f].name, d[f].value);
            if (b.extraData) {
                var g = c(b.extraData);
                for (f = 0; f < g.length; f++) g[f] && e.append(g[f][0], g[f][1])
            }
            b.data = null;
            var h = a.extend(!0, {}, a.ajaxSettings, b, {
                contentType: !1,
                processData: !1,
                cache: !1,
                type: i || "POST"
            });
            b.uploadProgress && (h.xhr = function() {
                var c = a.ajaxSettings.xhr();
                return c.upload && c.upload.addEventListener("progress", function(a) {
                    var c = 0,
                        d = a.loaded || a.position,
                        e = a.total;
                    a.lengthComputable && (c = Math.ceil(d / e * 100)), b.uploadProgress(a, d, e, c)
                }, !1), c
            }), h.data = null;
            var j = h.beforeSend;
            return h.beforeSend = function(a, c) {
                c.data = b.formData ? b.formData : e, j && j.call(this, a, c)
            }, a.ajax(h)
        }

        function h(c) {
            function e(a) {
                var b = null;
                try {
                    a.contentWindow && (b = a.contentWindow.document)
                } catch (a) {
                    d("cannot get iframe.contentWindow document: " + a)
                }
                if (b) return b;
                try {
                    b = a.contentDocument ? a.contentDocument : a.document
                } catch (c) {
                    d("cannot get iframe.contentDocument: " + c), b = a.document
                }
                return b
            }

            function g() {
                function b() {
                    try {
                        var a = e(r).readyState;
                        d("state = " + a), a && "uninitialized" == a.toLowerCase() && setTimeout(b, 50)
                    } catch (a) {
                        d("Server abort: ", a, " (", a.name, ")"), h(A), w && clearTimeout(w), w = void 0
                    }
                }
                var c = l.attr2("target"),
                    f = l.attr2("action"),
                    g = "multipart/form-data",
                    j = l.attr("enctype") || l.attr("encoding") || g;
                x.setAttribute("target", o), (!i || /post/i.test(i)) && x.setAttribute("method", "POST"), f != m.url && x.setAttribute("action", m.url), m.skipEncodingOverride || i && !/post/i.test(i) || l.attr({
                    encoding: "multipart/form-data",
                    enctype: "multipart/form-data"
                }), m.timeout && (w = setTimeout(function() {
                    v = !0, h(z)
                }, m.timeout));
                var k = [];
                try {
                    if (m.extraData)
                        for (var n in m.extraData) m.extraData.hasOwnProperty(n) && k.push(a.isPlainObject(m.extraData[n]) && m.extraData[n].hasOwnProperty("name") && m.extraData[n].hasOwnProperty("value") ? a('<input type="hidden" name="' + m.extraData[n].name + '">').val(m.extraData[n].value).appendTo(x)[0] : a('<input type="hidden" name="' + n + '">').val(m.extraData[n]).appendTo(x)[0]);
                    m.iframeTarget || q.appendTo("body"), r.attachEvent ? r.attachEvent("onload", h) : r.addEventListener("load", h, !1), setTimeout(b, 15);
                    try {
                        x.submit()
                    } catch (a) {
                        var p = document.createElement("form").submit;
                        p.apply(x)
                    }
                } finally {
                    x.setAttribute("action", f), x.setAttribute("enctype", j), c ? x.setAttribute("target", c) : l.removeAttr("target"), a(k).remove()
                }
            }

            function h(b) {
                if (!s.aborted && !F) {
                    if (E = e(r), E || (d("cannot access response document"), b = A), b === z && s) return s.abort("timeout"), void y.reject(s, "timeout");
                    if (b == A && s) return s.abort("server abort"), void y.reject(s, "error", "server abort");
                    if (E && E.location.href != m.iframeSrc || v) {
                        r.detachEvent ? r.detachEvent("onload", h) : r.removeEventListener("load", h, !1);
                        var c, f = "success";
                        try {
                            if (v) throw "timeout";
                            var g = "xml" == m.dataType || E.XMLDocument || a.isXMLDoc(E);
                            if (d("isXml=" + g), !g && window.opera && (null === E.body || !E.body.innerHTML) && --G) return d("requeing onLoad callback, DOM not available"), void setTimeout(h, 250);
                            var i = E.body ? E.body : E.documentElement;
                            s.responseText = i ? i.innerHTML : null, s.responseXML = E.XMLDocument ? E.XMLDocument : E, g && (m.dataType = "xml"), s.getResponseHeader = function(a) {
                                var b = {
                                    "content-type": m.dataType
                                };
                                return b[a.toLowerCase()]
                            }, i && (s.status = Number(i.getAttribute("status")) || s.status, s.statusText = i.getAttribute("statusText") || s.statusText);
                            var j = (m.dataType || "").toLowerCase(),
                                k = /(json|script|text)/.test(j);
                            if (k || m.textarea) {
                                var l = E.getElementsByTagName("textarea")[0];
                                if (l) s.responseText = l.value, s.status = Number(l.getAttribute("status")) || s.status, s.statusText = l.getAttribute("statusText") || s.statusText;
                                else if (k) {
                                    var o = E.getElementsByTagName("pre")[0],
                                        p = E.getElementsByTagName("body")[0];
                                    o ? s.responseText = o.textContent ? o.textContent : o.innerText : p && (s.responseText = p.textContent ? p.textContent : p.innerText)
                                }
                            } else "xml" == j && !s.responseXML && s.responseText && (s.responseXML = H(s.responseText));
                            try {
                                D = J(s, j, m)
                            } catch (a) {
                                f = "parsererror", s.error = c = a || f
                            }
                        } catch (a) {
                            d("error caught: ", a), f = "error", s.error = c = a || f
                        }
                        s.aborted && (d("upload aborted"), f = null), s.status && (f = s.status >= 200 && s.status < 300 || 304 === s.status ? "success" : "error"), "success" === f ? (m.success && m.success.call(m.context, D, "success", s), y.resolve(s.responseText, "success", s), n && a.event.trigger("ajaxSuccess", [s, m])) : f && (void 0 === c && (c = s.statusText), m.error && m.error.call(m.context, s, f, c), y.reject(s, "error", c), n && a.event.trigger("ajaxError", [s, m, c])), n && a.event.trigger("ajaxComplete", [s, m]), n && !--a.active && a.event.trigger("ajaxStop"), m.complete && m.complete.call(m.context, s, f), F = !0, m.timeout && clearTimeout(w), setTimeout(function() {
                            m.iframeTarget ? q.attr("src", m.iframeSrc) : q.remove(), s.responseXML = null
                        }, 100)
                    }
                }
            }
            var j, k, m, n, o, q, r, s, t, u, v, w, x = l[0],
                y = a.Deferred();
            if (y.abort = function(a) {
                    s.abort(a)
                }, c)
                for (k = 0; k < p.length; k++) j = a(p[k]), f ? j.prop("disabled", !1) : j.removeAttr("disabled");
            if (m = a.extend(!0, {}, a.ajaxSettings, b), m.context = m.context || m, o = "jqFormIO" + (new Date).getTime(), m.iframeTarget ? (q = a(m.iframeTarget), u = q.attr2("name"), u ? o = u : q.attr2("name", o)) : (q = a('<iframe name="' + o + '" src="' + m.iframeSrc + '" />'), q.css({
                    position: "absolute",
                    top: "-1000px",
                    left: "-1000px"
                })), r = q[0], s = {
                    aborted: 0,
                    responseText: null,
                    responseXML: null,
                    status: 0,
                    statusText: "n/a",
                    getAllResponseHeaders: function() {},
                    getResponseHeader: function() {},
                    setRequestHeader: function() {},
                    abort: function(b) {
                        var c = "timeout" === b ? "timeout" : "aborted";
                        d("aborting upload... " + c), this.aborted = 1;
                        try {
                            r.contentWindow.document.execCommand && r.contentWindow.document.execCommand("Stop")
                        } catch (a) {}
                        q.attr("src", m.iframeSrc), s.error = c, m.error && m.error.call(m.context, s, c, b), n && a.event.trigger("ajaxError", [s, m, c]), m.complete && m.complete.call(m.context, s, c)
                    }
                }, n = m.global, n && 0 === a.active++ && a.event.trigger("ajaxStart"), n && a.event.trigger("ajaxSend", [s, m]), m.beforeSend && m.beforeSend.call(m.context, s, m) === !1) return m.global && a.active--, y.reject(), y;
            if (s.aborted) return y.reject(), y;
            t = x.clk, t && (u = t.name, u && !t.disabled && (m.extraData = m.extraData || {}, m.extraData[u] = t.value, "image" == t.type && (m.extraData[u + ".x"] = x.clk_x, m.extraData[u + ".y"] = x.clk_y)));
            var z = 1,
                A = 2,
                B = a("meta[name=csrf-token]").attr("content"),
                C = a("meta[name=csrf-param]").attr("content");
            C && B && (m.extraData = m.extraData || {}, m.extraData[C] = B), m.forceSync ? g() : setTimeout(g, 10);
            var D, E, F, G = 50,
                H = a.parseXML || function(a, b) {
                    return window.ActiveXObject ? (b = new ActiveXObject("Microsoft.XMLDOM"), b.async = "false", b.loadXML(a)) : b = (new DOMParser).parseFromString(a, "text/xml"), b && b.documentElement && "parsererror" != b.documentElement.nodeName ? b : null
                },
                I = a.parseJSON || function(a) {
                    return window.eval("(" + a + ")")
                },
                J = function(b, c, d) {
                    var e = b.getResponseHeader("content-type") || "",
                        f = "xml" === c || !c && e.indexOf("xml") >= 0,
                        g = f ? b.responseXML : b.responseText;
                    return f && "parsererror" === g.documentElement.nodeName && a.error && a.error("parsererror"), d && d.dataFilter && (g = d.dataFilter(g, c)), "string" == typeof g && ("json" === c || !c && e.indexOf("json") >= 0 ? g = I(g) : ("script" === c || !c && e.indexOf("javascript") >= 0) && a.globalEval(g)), g
                };
            return y
        }
        if (!this.length) return d("ajaxSubmit: skipping submit process - no element selected"), this;
        var i, j, k, l = this;
        "function" == typeof b ? b = {
            success: b
        } : void 0 === b && (b = {}), i = b.type || this.attr2("method"), j = b.url || this.attr2("action"), k = "string" == typeof j ? a.trim(j) : "", k = k || window.location.href || "", k && (k = (k.match(/^([^#]+)/) || [])[1]), b = a.extend(!0, {
            url: k,
            success: a.ajaxSettings.success,
            type: i || a.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
        }, b);
        var m = {};
        if (this.trigger("form-pre-serialize", [this, b, m]), m.veto) return d("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
        if (b.beforeSerialize && b.beforeSerialize(this, b) === !1) return d("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
        var n = b.traditional;
        void 0 === n && (n = a.ajaxSettings.traditional);
        var o, p = [],
            q = this.formToArray(b.semantic, p);
        if (b.data && (b.extraData = b.data, o = a.param(b.data, n)), b.beforeSubmit && b.beforeSubmit(q, this, b) === !1) return d("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
        if (this.trigger("form-submit-validate", [q, this, b, m]), m.veto) return d("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
        var r = a.param(q, n);
        o && (r = r ? r + "&" + o : o), "GET" == b.type.toUpperCase() ? (b.url += (b.url.indexOf("?") >= 0 ? "&" : "?") + r, b.data = null) : b.data = r;
        var s = [];
        if (b.resetForm && s.push(function() {
                l.resetForm()
            }), b.clearForm && s.push(function() {
                l.clearForm(b.includeHidden)
            }), !b.dataType && b.target) {
            var t = b.success || function() {};
            s.push(function(c) {
                var d = b.replaceTarget ? "replaceWith" : "html";
                a(b.target)[d](c).each(t, arguments)
            })
        } else b.success && s.push(b.success);
        if (b.success = function(a, c, d) {
                for (var e = b.context || this, f = 0, g = s.length; g > f; f++) s[f].apply(e, [a, c, d || l, l])
            }, b.error) {
            var u = b.error;
            b.error = function(a, c, d) {
                var e = b.context || this;
                u.apply(e, [a, c, d, l])
            }
        }
        if (b.complete) {
            var v = b.complete;
            b.complete = function(a, c) {
                var d = b.context || this;
                v.apply(d, [a, c, l])
            }
        }
        var w = a("input[type=file]:enabled", this).filter(function() {
                return "" !== a(this).val()
            }),
            x = w.length > 0,
            y = "multipart/form-data",
            z = l.attr("enctype") == y || l.attr("encoding") == y,
            A = e.fileapi && e.formdata;
        d("fileAPI :" + A);
        var B, C = (x || z) && !A;
        b.iframe !== !1 && (b.iframe || C) ? b.closeKeepAlive ? a.get(b.closeKeepAlive, function() {
            B = h(q)
        }) : B = h(q) : B = (x || z) && A ? g(q) : a.ajax(b), l.removeData("jqxhr").data("jqxhr", B);
        for (var D = 0; D < p.length; D++) p[D] = null;
        return this.trigger("form-submit-notify", [this, b]), this
    }, a.fn.ajaxForm = function(e) {
        if (e = e || {}, e.delegation = e.delegation && a.isFunction(a.fn.on), !e.delegation && 0 === this.length) {
            var f = {
                s: this.selector,
                c: this.context
            };
            return !a.isReady && f.s ? (d("DOM not ready, queuing ajaxForm"), a(function() {
                a(f.s, f.c).ajaxForm(e)
            }), this) : (d("terminating; zero elements found by selector" + (a.isReady ? "" : " (DOM not ready)")), this)
        }
        return e.delegation ? (a(document).off("submit.form-plugin", this.selector, b).off("click.form-plugin", this.selector, c).on("submit.form-plugin", this.selector, e, b).on("click.form-plugin", this.selector, e, c), this) : this.ajaxFormUnbind().bind("submit.form-plugin", e, b).bind("click.form-plugin", e, c)
    }, a.fn.ajaxFormUnbind = function() {
        return this.unbind("submit.form-plugin click.form-plugin")
    }, a.fn.formToArray = function(b, c) {
        var d = [];
        if (0 === this.length) return d;
        var f, g = this[0],
            h = this.attr("id"),
            i = b ? g.getElementsByTagName("*") : g.elements;
        if (i && !/MSIE [678]/.test(navigator.userAgent) && (i = a(i).get()), h && (f = a(':input[form="' + h + '"]').get(), f.length && (i = (i || []).concat(f))), !i || !i.length) return d;
        var j, k, l, m, n, o, p;
        for (j = 0, o = i.length; o > j; j++)
            if (n = i[j], l = n.name, l && !n.disabled)
                if (b && g.clk && "image" == n.type) g.clk == n && (d.push({
                    name: l,
                    value: a(n).val(),
                    type: n.type
                }), d.push({
                    name: l + ".x",
                    value: g.clk_x
                }, {
                    name: l + ".y",
                    value: g.clk_y
                }));
                else if (m = a.fieldValue(n, !0), m && m.constructor == Array)
            for (c && c.push(n), k = 0, p = m.length; p > k; k++) d.push({
                name: l,
                value: m[k]
            });
        else if (e.fileapi && "file" == n.type) {
            c && c.push(n);
            var q = n.files;
            if (q.length)
                for (k = 0; k < q.length; k++) d.push({
                    name: l,
                    value: q[k],
                    type: n.type
                });
            else d.push({
                name: l,
                value: "",
                type: n.type
            })
        } else null !== m && "undefined" != typeof m && (c && c.push(n), d.push({
            name: l,
            value: m,
            type: n.type,
            required: n.required
        }));
        if (!b && g.clk) {
            var r = a(g.clk),
                s = r[0];
            l = s.name, l && !s.disabled && "image" == s.type && (d.push({
                name: l,
                value: r.val()
            }), d.push({
                name: l + ".x",
                value: g.clk_x
            }, {
                name: l + ".y",
                value: g.clk_y
            }))
        }
        return d
    }, a.fn.formSerialize = function(b) {
        return a.param(this.formToArray(b))
    }, a.fn.fieldSerialize = function(b) {
        var c = [];
        return this.each(function() {
            var d = this.name;
            if (d) {
                var e = a.fieldValue(this, b);
                if (e && e.constructor == Array)
                    for (var f = 0, g = e.length; g > f; f++) c.push({
                        name: d,
                        value: e[f]
                    });
                else null !== e && "undefined" != typeof e && c.push({
                    name: this.name,
                    value: e
                })
            }
        }), a.param(c)
    }, a.fn.fieldValue = function(b) {
        for (var c = [], d = 0, e = this.length; e > d; d++) {
            var f = this[d],
                g = a.fieldValue(f, b);
            null === g || "undefined" == typeof g || g.constructor == Array && !g.length || (g.constructor == Array ? a.merge(c, g) : c.push(g))
        }
        return c
    }, a.fieldValue = function(b, c) {
        var d = b.name,
            e = b.type,
            f = b.tagName.toLowerCase();
        if (void 0 === c && (c = !0), c && (!d || b.disabled || "reset" == e || "button" == e || ("checkbox" == e || "radio" == e) && !b.checked || ("submit" == e || "image" == e) && b.form && b.form.clk != b || "select" == f && -1 == b.selectedIndex)) return null;
        if ("select" == f) {
            var g = b.selectedIndex;
            if (0 > g) return null;
            for (var h = [], i = b.options, j = "select-one" == e, k = j ? g + 1 : i.length, l = j ? g : 0; k > l; l++) {
                var m = i[l];
                if (m.selected) {
                    var n = m.value;
                    if (n || (n = m.attributes && m.attributes.value && !m.attributes.value.specified ? m.text : m.value), j) return n;
                    h.push(n)
                }
            }
            return h
        }
        return a(b).val()
    }, a.fn.clearForm = function(b) {
        return this.each(function() {
            a("input,select,textarea", this).clearFields(b)
        })
    }, a.fn.clearFields = a.fn.clearInputs = function(b) {
        var c = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function() {
            var d = this.type,
                e = this.tagName.toLowerCase();
            c.test(d) || "textarea" == e ? this.value = "" : "checkbox" == d || "radio" == d ? this.checked = !1 : "select" == e ? this.selectedIndex = -1 : "file" == d ? /MSIE/.test(navigator.userAgent) ? a(this).replaceWith(a(this).clone(!0)) : a(this).val("") : b && (b === !0 && /hidden/.test(d) || "string" == typeof b && a(this).is(b)) && (this.value = "")
        })
    }, a.fn.resetForm = function() {
        return this.each(function() {
            ("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset()
        })
    }, a.fn.enable = function(a) {
        return void 0 === a && (a = !0), this.each(function() {
            this.disabled = !a
        })
    }, a.fn.selected = function(b) {
        return void 0 === b && (b = !0), this.each(function() {
            var c = this.type;
            if ("checkbox" == c || "radio" == c) this.checked = b;
            else if ("option" == this.tagName.toLowerCase()) {
                var d = a(this).parent("select");
                b && d[0] && "select-one" == d[0].type && d.find("option").selected(!1), this.selected = b
            }
        })
    }, a.fn.ajaxSubmit.debug = !1
}),
function(a, b, c) {
    function d(c, d, e) {
        var f = b.createElement(c);
        return d && (f.id = _ + d), e && (f.style.cssText = e), a(f)
    }

    function e() {
        return c.innerHeight ? c.innerHeight : a(c).height()
    }

    function f(b, c) {
        c !== Object(c) && (c = {}), this.cache = {}, this.el = b, this.value = function(b) {
            var d;
            return void 0 === this.cache[b] && (d = a(this.el).attr("data-cbox-" + b), void 0 !== d ? this.cache[b] = d : void 0 !== c[b] ? this.cache[b] = c[b] : void 0 !== Z[b] && (this.cache[b] = Z[b])), this.cache[b]
        }, this.get = function(b) {
            var c = this.value(b);
            return a.isFunction(c) ? c.call(this.el, this) : c
        }
    }

    function g(a) {
        var b = A.length,
            c = (R + a) % b;
        return 0 > c ? b + c : c
    }

    function h(a, b) {
        return Math.round((/%/.test(a) ? ("x" === b ? B.width() : e()) / 100 : 1) * parseInt(a, 10))
    }

    function i(a, b) {
        return a.get("photo") || a.get("photoRegex").test(b)
    }

    function j(a, b) {
        return a.get("retinaUrl") && c.devicePixelRatio > 1 ? b.replace(a.get("photoRegex"), a.get("retinaSuffix")) : b
    }

    function k(a) {
        "contains" in t[0] && !t[0].contains(a.target) && a.target !== s[0] && (a.stopPropagation(), t.focus())
    }

    function l(a) {
        l.str !== a && (t.add(s).removeClass(l.str).addClass(a), l.str = a)
    }

    function m(b) {
        R = 0, b && b !== !1 && "nofollow" !== b ? (A = a("." + aa).filter(function() {
            var c = a.data(this, $),
                d = new f(this, c);
            return d.get("rel") === b
        }), R = A.index(M.el), -1 === R && (A = A.add(M.el), R = A.length - 1)) : A = a(M.el)
    }

    function n(c) {
        a(b).trigger(c), ha.triggerHandler(c)
    }

    function o(c) {
        var e;
        if (!V) {
            if (e = a(c).data($), M = new f(c, e), m(M.get("rel")), !T) {
                T = U = !0, l(M.get("className")), t.css({
                    visibility: "hidden",
                    display: "block",
                    opacity: ""
                }), C = d(ia, "LoadedContent", "width:0; height:0; overflow:hidden; visibility:hidden"), v.css({
                    width: "",
                    height: ""
                }).append(C), N = w.height() + z.height() + v.outerHeight(!0) - v.height(), O = x.width() + y.width() + v.outerWidth(!0) - v.width(), P = C.outerHeight(!0), Q = C.outerWidth(!0);
                var g = h(M.get("initialWidth"), "x"),
                    i = h(M.get("initialHeight"), "y"),
                    j = M.get("maxWidth"),
                    o = M.get("maxHeight");
                M.w = (j !== !1 ? Math.min(g, h(j, "x")) : g) - Q - O, M.h = (o !== !1 ? Math.min(i, h(o, "y")) : i) - P - N, C.css({
                    width: "",
                    height: M.h
                }), X.position(), n(ba), M.get("onOpen"), L.add(F).hide(), t.focus(), M.get("trapFocus") && b.addEventListener && (b.addEventListener("focus", k, !0), ha.one(fa, function() {
                    b.removeEventListener("focus", k, !0)
                })), M.get("returnFocus") && ha.one(fa, function() {
                    a(M.el).focus()
                })
            }
            var p = parseFloat(M.get("opacity"));
            s.css({
                opacity: p === p ? p : "",
                cursor: M.get("overlayClose") ? "pointer" : "",
                visibility: "visible"
            }).show(), M.get("closeButton") ? K.html(M.get("close")).appendTo(v) : K.appendTo("<div/>"), r()
        }
    }

    function p() {
        t || (Y = !1, B = a(c), t = d(ia).attr({
            id: $,
            class: a.support.opacity === !1 ? _ + "IE" : "",
            role: "dialog",
            tabindex: "-1"
        }).hide(), s = d(ia, "Overlay").hide(), E = a([d(ia, "LoadingOverlay")[0], d(ia, "LoadingGraphic")[0]]), u = d(ia, "Wrapper"), v = d(ia, "Content").append(F = d(ia, "Title"), G = d(ia, "Current"), J = a('<button type="button"/>').attr({
            id: _ + "Previous"
        }), I = a('<button type="button"/>').attr({
            id: _ + "Next"
        }), H = d("button", "Slideshow"), E), K = a('<button type="button"/>').attr({
            id: _ + "Close"
        }), u.append(d(ia).append(d(ia, "TopLeft"), w = d(ia, "TopCenter"), d(ia, "TopRight")), d(ia, !1, "clear:left").append(x = d(ia, "MiddleLeft"), v, y = d(ia, "MiddleRight")), d(ia, !1, "clear:left").append(d(ia, "BottomLeft"), z = d(ia, "BottomCenter"), d(ia, "BottomRight"))).find("div div").css({
            float: "left"
        }), D = d(ia, !1, "position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;"), L = I.add(J).add(G).add(H)), b.body && !t.parent().length && a(b.body).append(s, t.append(u, D))
    }

    function q() {
        function c(a) {
            a.which > 1 || a.shiftKey || a.altKey || a.metaKey || a.ctrlKey || (a.preventDefault(), o(this))
        }
        return !!t && (Y || (Y = !0, I.click(function() {
            X.next()
        }), J.click(function() {
            X.prev()
        }), K.click(function() {
            X.close()
        }), s.click(function() {
            M.get("overlayClose") && X.close()
        }), a(b).bind("keydown." + _, function(a) {
            var b = a.keyCode;
            T && M.get("escKey") && 27 === b && (a.preventDefault(), X.close()), T && M.get("arrowKey") && A[1] && !a.altKey && (37 === b ? (a.preventDefault(), J.click()) : 39 === b && (a.preventDefault(), I.click()))
        }), a.isFunction(a.fn.on) ? a(b).on("click." + _, "." + aa, c) : a("." + aa).live("click." + _, c)), !0)
    }

    function r() {
        var b, e, f, g = X.prep,
            k = ++ja;
        if (U = !0, S = !1, n(ga), n(ca), M.get("onLoad"), M.h = M.get("height") ? h(M.get("height"), "y") - P - N : M.get("innerHeight") && h(M.get("innerHeight"), "y"), M.w = M.get("width") ? h(M.get("width"), "x") - Q - O : M.get("innerWidth") && h(M.get("innerWidth"), "x"), M.mw = M.w, M.mh = M.h, M.get("maxWidth") && (M.mw = h(M.get("maxWidth"), "x") - Q - O, M.mw = M.w && M.w < M.mw ? M.w : M.mw), M.get("maxHeight") && (M.mh = h(M.get("maxHeight"), "y") - P - N, M.mh = M.h && M.h < M.mh ? M.h : M.mh), b = M.get("href"), W = setTimeout(function() {
                E.show()
            }, 100), M.get("inline")) {
            var l = a(b);
            f = a("<div>").hide().insertBefore(l), ha.one(ga, function() {
                f.replaceWith(l)
            }), g(l)
        } else M.get("iframe") ? g(" ") : M.get("html") ? g(M.get("html")) : i(M, b) ? (b = j(M, b), S = new Image, a(S).addClass(_ + "Photo").bind("error", function() {
            g(d(ia, "Error").html(M.get("imgError")))
        }).one("load", function() {
            k === ja && setTimeout(function() {
                var b;
                a.each(["alt", "longdesc", "aria-describedby"], function(b, c) {
                    var d = a(M.el).attr(c) || a(M.el).attr("data-" + c);
                    d && S.setAttribute(c, d)
                }), M.get("retinaImage") && c.devicePixelRatio > 1 && (S.height = S.height / c.devicePixelRatio, S.width = S.width / c.devicePixelRatio), M.get("scalePhotos") && (e = function() {
                    S.height -= S.height * b, S.width -= S.width * b
                }, M.mw && S.width > M.mw && (b = (S.width - M.mw) / S.width, e()), M.mh && S.height > M.mh && (b = (S.height - M.mh) / S.height, e())), M.h && (S.style.marginTop = Math.max(M.mh - S.height, 0) / 2 + "px"), A[1] && (M.get("loop") || A[R + 1]) && (S.style.cursor = "pointer", S.onclick = function() {
                    X.next()
                }), S.style.width = S.width + "px", S.style.height = S.height + "px", g(S)
            }, 1)
        }), S.src = b) : b && D.load(b, M.get("data"), function(b, c) {
            k === ja && g("error" === c ? d(ia, "Error").html(M.get("xhrError")) : a(this).contents())
        })
    }
    var s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z = {
            html: !1,
            photo: !1,
            iframe: !1,
            inline: !1,
            transition: "elastic",
            speed: 300,
            fadeOut: 300,
            width: !1,
            initialWidth: "600",
            innerWidth: !1,
            maxWidth: !1,
            height: !1,
            initialHeight: "450",
            innerHeight: !1,
            maxHeight: !1,
            scalePhotos: !0,
            scrolling: !0,
            opacity: .9,
            preloading: !0,
            className: !1,
            overlayClose: !0,
            escKey: !0,
            arrowKey: !0,
            top: !1,
            bottom: !1,
            left: !1,
            right: !1,
            fixed: !1,
            data: void 0,
            closeButton: !0,
            fastIframe: !0,
            open: !1,
            reposition: !0,
            loop: !0,
            slideshow: !1,
            slideshowAuto: !0,
            slideshowSpeed: 2500,
            slideshowStart: "start slideshow",
            slideshowStop: "stop slideshow",
            photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,
            retinaImage: !1,
            retinaUrl: !1,
            retinaSuffix: "@2x.$1",
            current: "image {current} of {total}",
            previous: "previous",
            next: "next",
            close: "close",
            xhrError: "This content failed to load.",
            imgError: "This image failed to load.",
            returnFocus: !0,
            trapFocus: !0,
            onOpen: !1,
            onLoad: !1,
            onComplete: !1,
            onCleanup: !1,
            onClosed: !1,
            rel: function() {
                return this.rel
            },
            href: function() {
                return a(this).attr("href")
            },
            title: function() {
                return this.title
            }
        },
        $ = "colorbox",
        _ = "cbox",
        aa = _ + "Element",
        ba = _ + "_open",
        ca = _ + "_load",
        da = _ + "_complete",
        ea = _ + "_cleanup",
        fa = _ + "_closed",
        ga = _ + "_purge",
        ha = a("<a/>"),
        ia = "div",
        ja = 0,
        ka = {},
        la = function() {
            function a() {
                clearTimeout(g)
            }

            function b() {
                (M.get("loop") || A[R + 1]) && (a(), g = setTimeout(X.next, M.get("slideshowSpeed")))
            }

            function c() {
                H.html(M.get("slideshowStop")).unbind(i).one(i, d), ha.bind(da, b).bind(ca, a), t.removeClass(h + "off").addClass(h + "on")
            }

            function d() {
                a(), ha.unbind(da, b).unbind(ca, a), H.html(M.get("slideshowStart")).unbind(i).one(i, function() {
                    X.next(), c()
                }), t.removeClass(h + "on").addClass(h + "off")
            }

            function e() {
                f = !1, H.hide(), a(), ha.unbind(da, b).unbind(ca, a), t.removeClass(h + "off " + h + "on")
            }
            var f, g, h = _ + "Slideshow_",
                i = "click." + _;
            return function() {
                f ? M.get("slideshow") || (ha.unbind(ea, e), e()) : M.get("slideshow") && A[1] && (f = !0, ha.one(ea, e), M.get("slideshowAuto") ? c() : d(), H.show())
            }
        }();
    a[$] || (a(p), X = a.fn[$] = a[$] = function(b, c) {
        var d, e = this;
        if (b = b || {}, a.isFunction(e)) e = a("<a/>"), b.open = !0;
        else if (!e[0]) return e;
        return e[0] ? (p(), q() && (c && (b.onComplete = c), e.each(function() {
            var c = a.data(this, $) || {};
            a.data(this, $, a.extend(c, b))
        }).addClass(aa), d = new f(e[0], b), d.get("open") && o(e[0])), e) : e
    }, X.position = function(b, c) {
        function d() {
            w[0].style.width = z[0].style.width = v[0].style.width = parseInt(t[0].style.width, 10) - O + "px", v[0].style.height = x[0].style.height = y[0].style.height = parseInt(t[0].style.height, 10) - N + "px"
        }
        var f, g, i, j = 0,
            k = 0,
            l = t.offset();
        if (B.unbind("resize." + _), t.css({
                top: -9e4,
                left: -9e4
            }), g = B.scrollTop(), i = B.scrollLeft(), M.get("fixed") ? (l.top -= g, l.left -= i, t.css({
                position: "fixed"
            })) : (j = g, k = i, t.css({
                position: "absolute"
            })), k += M.get("right") !== !1 ? Math.max(B.width() - M.w - Q - O - h(M.get("right"), "x"), 0) : M.get("left") !== !1 ? h(M.get("left"), "x") : Math.round(Math.max(B.width() - M.w - Q - O, 0) / 2), j += M.get("bottom") !== !1 ? Math.max(e() - M.h - P - N - h(M.get("bottom"), "y"), 0) : M.get("top") !== !1 ? h(M.get("top"), "y") : Math.round(Math.max(e() - M.h - P - N, 0) / 2), t.css({
                top: l.top,
                left: l.left,
                visibility: "visible"
            }), u[0].style.width = u[0].style.height = "9999px", f = {
                width: M.w + Q + O,
                height: M.h + P + N,
                top: j,
                left: k
            }, b) {
            var m = 0;
            a.each(f, function(a) {
                return f[a] !== ka[a] ? void(m = b) : void 0
            }), b = m
        }
        ka = f, b || t.css(f), t.dequeue().animate(f, {
            duration: b || 0,
            complete: function() {
                d(), U = !1, u[0].style.width = M.w + Q + O + "px", u[0].style.height = M.h + P + N + "px", M.get("reposition") && setTimeout(function() {
                    B.bind("resize." + _, X.position)
                }, 1), a.isFunction(c) && c()
            },
            step: d
        })
    }, X.resize = function(a) {
        var b;
        T && (a = a || {}, a.width && (M.w = h(a.width, "x") - Q - O), a.innerWidth && (M.w = h(a.innerWidth, "x")), C.css({
            width: M.w
        }), a.height && (M.h = h(a.height, "y") - P - N), a.innerHeight && (M.h = h(a.innerHeight, "y")), a.innerHeight || a.height || (b = C.scrollTop(), C.css({
            height: "auto"
        }), M.h = C.height()), C.css({
            height: M.h
        }), b && C.scrollTop(b), X.position("none" === M.get("transition") ? 0 : M.get("speed")))
    }, X.prep = function(c) {
        function e() {
            return M.w = M.w || C.width(), M.w = M.mw && M.mw < M.w ? M.mw : M.w, M.w
        }

        function h() {
            return M.h = M.h || C.height(), M.h = M.mh && M.mh < M.h ? M.mh : M.h, M.h
        }
        if (T) {
            var k, m = "none" === M.get("transition") ? 0 : M.get("speed");
            C.remove(), C = d(ia, "LoadedContent").append(c), C.hide().appendTo(D.show()).css({
                width: e(),
                overflow: M.get("scrolling") ? "auto" : "hidden"
            }).css({
                height: h()
            }).prependTo(v), D.hide(), a(S).css({
                float: "none"
            }), l(M.get("className")), k = function() {
                function c() {
                    a.support.opacity === !1 && t[0].style.removeAttribute("filter")
                }
                var d, e, h = A.length;
                T && (e = function() {
                    clearTimeout(W), E.hide(), n(da), M.get("onComplete")
                }, F.html(M.get("title")).show(), C.show(), h > 1 ? ("string" == typeof M.get("current") && G.html(M.get("current").replace("{current}", R + 1).replace("{total}", h)).show(), I[M.get("loop") || h - 1 > R ? "show" : "hide"]().html(M.get("next")), J[M.get("loop") || R ? "show" : "hide"]().html(M.get("previous")), la(), M.get("preloading") && a.each([g(-1), g(1)], function() {
                    var c, d = A[this],
                        e = new f(d, a.data(d, $)),
                        g = e.get("href");
                    g && i(e, g) && (g = j(e, g), c = b.createElement("img"), c.src = g)
                })) : L.hide(), M.get("iframe") ? (d = b.createElement("iframe"), "frameBorder" in d && (d.frameBorder = 0), "allowTransparency" in d && (d.allowTransparency = "true"), M.get("scrolling") || (d.scrolling = "no"), a(d).attr({
                    src: M.get("href"),
                    name: (new Date).getTime(),
                    class: _ + "Iframe",
                    allowFullScreen: !0
                }).one("load", e).appendTo(C), ha.one(ga, function() {
                    d.src = "//about:blank"
                }), M.get("fastIframe") && a(d).trigger("load")) : e(), "fade" === M.get("transition") ? t.fadeTo(m, 1, c) : c())
            }, "fade" === M.get("transition") ? t.fadeTo(m, 0, function() {
                X.position(0, k)
            }) : X.position(m, k)
        }
    }, X.next = function() {
        !U && A[1] && (M.get("loop") || A[R + 1]) && (R = g(1), o(A[R]))
    }, X.prev = function() {
        !U && A[1] && (M.get("loop") || R) && (R = g(-1), o(A[R]))
    }, X.close = function() {
        T && !V && (V = !0, T = !1, n(ea), M.get("onCleanup"), B.unbind("." + _), s.fadeTo(M.get("fadeOut") || 0, 0), t.stop().fadeTo(M.get("fadeOut") || 0, 0, function() {
            t.hide(), s.hide(), n(ga), C.remove(), setTimeout(function() {
                V = !1, n(fa), M.get("onClosed")
            }, 1)
        }))
    }, X.remove = function() {
        t && (t.stop(), a[$].close(), t.stop(!1, !0).remove(), s.remove(), V = !1, t = null, a("." + aa).removeData($).removeClass(aa), a(b).unbind("click." + _).unbind("keydown." + _))
    }, X.element = function() {
        return a(M.el)
    }, X.settings = Z)
}(jQuery, document, window), ! function(a) {
    var b, c = {
            className: "autosizejs",
            id: "autosizejs",
            append: "\n",
            callback: !1,
            resizeDelay: 10,
            placeholder: !0
        },
        d = '<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',
        e = ["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform", "wordSpacing", "textIndent", "whiteSpace"],
        f = a(d).data("autosize", !0)[0];
    f.style.lineHeight = "99px", "99px" === a(f).css("lineHeight") && e.push("lineHeight"), f.style.lineHeight = "", a.fn.autosize = function(d) {
        return this.length ? (d = a.extend({}, c, d || {}), f.parentNode !== document.body && a(document.body).append(f), this.each(function() {
            function c() {
                var b, c = !!window.getComputedStyle && window.getComputedStyle(m, null);
                c ? (b = m.getBoundingClientRect().width, (0 === b || "number" != typeof b) && (b = parseFloat(c.width)), a.each(["paddingLeft", "paddingRight", "borderLeftWidth", "borderRightWidth"], function(a, d) {
                    b -= parseFloat(c[d])
                })) : b = n.width(), f.style.width = Math.max(b, 0) + "px"
            }

            function g() {
                var g = {};
                if (b = m, f.className = d.className, f.id = d.id, j = parseFloat(n.css("maxHeight")), a.each(e, function(a, b) {
                        g[b] = n.css(b)
                    }), a(f).css(g).attr("wrap", n.attr("wrap")), c(), window.chrome) {
                    var h = m.style.width;
                    m.style.width = "0px", m.offsetWidth, m.style.width = h
                }
            }

            function h() {
                var a, e;
                b !== m ? g() : c(), f.value = !m.value && d.placeholder ? n.attr("placeholder") || "" : m.value, f.value += d.append || "", f.style.overflowY = m.style.overflowY, e = parseFloat(m.style.height), f.scrollTop = 0, f.scrollTop = 9e4, a = f.scrollTop, j && a > j ? (m.style.overflowY = "scroll", a = j) : (m.style.overflowY = "hidden",
                    k > a && (a = k)), a += o, e !== a && (m.style.height = a + "px", f.className = f.className, p && d.callback.call(m, m), n.trigger("autosize.resized"))
            }

            function i() {
                clearTimeout(l), l = setTimeout(function() {
                    var a = n.width();
                    a !== r && (r = a, h())
                }, parseInt(d.resizeDelay, 10))
            }
            var j, k, l, m = this,
                n = a(m),
                o = 0,
                p = a.isFunction(d.callback),
                q = {
                    height: m.style.height,
                    overflow: m.style.overflow,
                    overflowY: m.style.overflowY,
                    wordWrap: m.style.wordWrap,
                    resize: m.style.resize
                },
                r = n.width(),
                s = n.css("resize");
            n.data("autosize") || (n.data("autosize", !0), ("border-box" === n.css("box-sizing") || "border-box" === n.css("-moz-box-sizing") || "border-box" === n.css("-webkit-box-sizing")) && (o = n.outerHeight() - n.height()), k = Math.max(parseFloat(n.css("minHeight")) - o || 0, n.height()), n.css({
                overflow: "hidden",
                overflowY: "hidden",
                wordWrap: "break-word"
            }), "vertical" === s ? n.css("resize", "none") : "both" === s && n.css("resize", "horizontal"), "onpropertychange" in m ? "oninput" in m ? n.on("input.autosize keyup.autosize", h) : n.on("propertychange.autosize", function() {
                "value" === event.propertyName && h()
            }) : n.on("input.autosize", h), d.resizeDelay !== !1 && a(window).on("resize.autosize", i), n.on("autosize.resize", h), n.on("autosize.resizeIncludeStyle", function() {
                b = null, h()
            }), n.on("autosize.destroy", function() {
                b = null, clearTimeout(l), a(window).off("resize", i), n.off("autosize").off(".autosize").css(q).removeData("autosize")
            }), h())
        })) : this
    }
}(jQuery || $), ! function(a) {
    a.fn.caret = function(a) {
        var b = this[0],
            c = "true" === b.contentEditable;
        if (0 == arguments.length) {
            if (window.getSelection) {
                if (c) {
                    b.focus();
                    var d = window.getSelection().getRangeAt(0),
                        e = d.cloneRange();
                    return e.selectNodeContents(b), e.setEnd(d.endContainer, d.endOffset), e.toString().length
                }
                return b.selectionStart
            }
            if (document.selection) {
                if (b.focus(), c) {
                    var d = document.selection.createRange(),
                        e = document.body.createTextRange();
                    return e.moveToElementText(b), e.setEndPoint("EndToEnd", d), e.text.length
                }
                var a = 0,
                    f = b.createTextRange(),
                    e = document.selection.createRange().duplicate(),
                    g = e.getBookmark();
                for (f.moveToBookmark(g); 0 !== f.moveStart("character", -1);) a++;
                return a
            }
            return 0
        }
        if (-1 == a && (a = this[c ? "text" : "val"]().length), window.getSelection) c ? (b.focus(), window.getSelection().collapse(b.firstChild, a)) : b.setSelectionRange(a, a);
        else if (document.body.createTextRange)
            if (c) {
                var f = document.body.createTextRange();
                f.moveToElementText(b), f.moveStart("character", a), f.collapse(!0), f.select()
            } else {
                var f = b.createTextRange();
                f.move("character", a), f.select()
            }
        return c || b.focus(), a
    }
}(jQuery), ! function(a) {
    a.fn.autoComplete = function(b) {
        var c = a.extend({}, a.fn.autoComplete.defaults, b);
        return "string" == typeof b ? (this.each(function() {
            var c = a(this);
            "destroy" == b && (a(window).off("resize.autocomplete", c.updateSC), c.off("blur.autocomplete focus.autocomplete keydown.autocomplete keyup.autocomplete"), c.data("autocomplete") ? c.attr("autocomplete", c.data("autocomplete")) : c.removeAttr("autocomplete"), a(c.data("sc")).remove(), c.removeData("sc").removeData("autocomplete"))
        }), this) : this.each(function() {
            function b(a) {
                var b = d.val();
                if (d.cache[b] = a, a.length && b.length >= c.minChars) {
                    for (var e = "", f = 0; f < a.length; f++) e += c.renderItem(a[f], b);
                    d.sc.html(e), d.updateSC(0)
                } else d.sc.hide()
            }
            var d = a(this);
            d.sc = a('<div class="autocomplete-suggestions ' + c.menuClass + '"></div>'), d.data("sc", d.sc).data("autocomplete", d.attr("autocomplete")), d.attr("autocomplete", "off"), d.cache = {}, d.last_val = "", d.updateSC = function(b, c) {
                if (d.sc.css({
                        top: d.offset().top + d.outerHeight(),
                        left: d.offset().left,
                        width: d.outerWidth() + 17
                    }), !b && (d.sc.show(), d.sc.maxHeight || (d.sc.maxHeight = parseInt(d.sc.css("max-height"))), d.sc.suggestionHeight || (d.sc.suggestionHeight = a(".autocomplete-suggestion", d.sc).first().outerHeight()), d.sc.suggestionHeight))
                    if (c) {
                        var e = d.sc.scrollTop(),
                            f = c.offset().top - d.sc.offset().top;
                        f + d.sc.suggestionHeight - d.sc.maxHeight > 0 ? d.sc.scrollTop(f + d.sc.suggestionHeight + e - d.sc.maxHeight) : 0 > f && d.sc.scrollTop(f + e)
                    } else d.sc.scrollTop(0)
            }, a(window).on("resize.autocomplete", d.updateSC), d.sc.appendTo("body"), d.sc.on("mouseleave", ".autocomplete-suggestion", function() {
                a(".autocomplete-suggestion.selected").removeClass("selected")
            }), d.sc.on("mouseenter", ".autocomplete-suggestion", function() {
                a(".autocomplete-suggestion.selected").removeClass("selected"), a(this).addClass("selected")
            }), d.sc.on("mousedown", ".autocomplete-suggestion", function(b) {
                var e = a(this),
                    f = e.data("val");
                return (f || e.hasClass("autocomplete-suggestion")) && (d.val(f), c.onSelect(b, f, e), d.sc.hide()), !1
            }), d.on("blur.autocomplete", function() {
                try {
                    over_sb = a(".autocomplete-suggestions:hover").length
                } catch (a) {
                    over_sb = 0
                }
                over_sb ? d.is(":focus") || setTimeout(function() {
                    d.focus()
                }, 20) : (d.last_val = d.val(), d.sc.hide(), setTimeout(function() {
                    d.sc.hide()
                }, 350))
            }), c.minChars || d.on("focus.autocomplete", function() {
                d.last_val = "\n", d.trigger("keyup.autocomplete")
            }), d.on("keydown.autocomplete", function(b) {
                if ((40 == b.which || 38 == b.which) && d.sc.html()) {
                    var e, f = a(".autocomplete-suggestion.selected", d.sc);
                    return f.length ? (e = 40 == b.which ? f.next(".autocomplete-suggestion") : f.prev(".autocomplete-suggestion"), e.length ? (f.removeClass("selected"), d.val(e.addClass("selected").data("val"))) : (f.removeClass("selected"), d.val(d.last_val), e = 0)) : (e = 40 == b.which ? a(".autocomplete-suggestion", d.sc).first() : a(".autocomplete-suggestion", d.sc).last(), d.val(e.addClass("selected").data("val"))), d.updateSC(0, e), !1
                }
                if (27 == b.which) d.val(d.last_val).sc.hide();
                else if (13 == b.which || 9 == b.which) {
                    var f = a(".autocomplete-suggestion.selected", d.sc);
                    f.length && d.sc.is(":visible") && (c.onSelect(b, f.data("val"), f), setTimeout(function() {
                        d.sc.hide()
                    }, 20))
                }
            }), d.on("keyup.autocomplete", function(e) {
                if (!~a.inArray(e.which, [13, 27, 35, 36, 37, 38, 39, 40])) {
                    var f = d.val();
                    if (f.length >= c.minChars) {
                        if (f != d.last_val) {
                            if (d.last_val = f, clearTimeout(d.timer), c.cache) {
                                if (f in d.cache) return void b(d.cache[f]);
                                for (var g = 1; g < f.length - c.minChars; g++) {
                                    var h = f.slice(0, f.length - g);
                                    if (h in d.cache && !d.cache[h].length) return void b([])
                                }
                            }
                            d.timer = setTimeout(function() {
                                c.source(f, b)
                            }, c.delay)
                        }
                    } else d.last_val = f, d.sc.hide()
                }
            })
        })
    }, a.fn.autoComplete.defaults = {
        source: 0,
        minChars: 3,
        delay: 150,
        cache: 1,
        menuClass: "",
        renderItem: function(a, b) {
            b = b.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
            var c = new RegExp("(" + b.split(" ").join("|") + ")", "gi");
            return '<div class="autocomplete-suggestion" data-val="' + a + '">' + a.replace(c, "<b>$1</b>") + "</div>"
        },
        onSelect: function(a, b, c) {}
    }
}(jQuery), ! function(a) {
    function b(a, c, d, e) {
        function f(a) {
            d.maxRows && l > d.maxRows || d.truncate && a && l > 1 ? n[g][0].style.display = "none" : (n[g][4] && (n[g][3].attr("src", n[g][4]), n[g][4] = ""), n[g][0].style.width = j + "px", n[g][0].style.height = p + "px", n[g][0].style.display = "block")
        }
        var g, j, k = 1,
            l = 1,
            m = a.width() - 2,
            n = [],
            o = 0,
            p = d.rowHeight;
        for (m || (m = a.width() - 2), i = 0; i < c.length; i++)
            if (n.push(c[i]), o += c[i][2] + d.margin, o >= m) {
                var q = n.length * d.margin;
                for (k = (m - q) / (o - q), p = Math.ceil(d.rowHeight * k), exact_w = 0, g = 0; g < n.length; g++) j = Math.ceil(n[g][2] * k), exact_w += j + d.margin, exact_w > m && (j -= exact_w - m), f();
                n = [], o = 0, l++
            }
        for (g = 0; g < n.length; g++) j = Math.floor(n[g][2] * k), h = Math.floor(d.rowHeight * k), f(!0);
        e || m == a.width() || b(a, c, d, !0)
    }
    a.fn.flexImages = function(c) {
        var d = a.extend({
            container: ".item",
            object: "img",
            rowHeight: 180,
            maxRows: 0,
            truncate: 0
        }, c);
        return this.each(function() {
            var c = a(this),
                e = a(c).find(d.container),
                f = [],
                g = (new Date).getTime(),
                h = window.getComputedStyle ? getComputedStyle(e[0], null) : e[0].currentStyle;
            for (d.margin = (parseInt(h.marginLeft) || 0) + (parseInt(h.marginRight) || 0) + (Math.round(parseFloat(h.borderLeftWidth)) || 0) + (Math.round(parseFloat(h.borderRightWidth)) || 0), j = 0; j < e.length; j++) {
                var i = e[j],
                    k = parseInt(i.getAttribute("data-w")),
                    l = k * (d.rowHeight / parseInt(i.getAttribute("data-h"))),
                    m = a(i).find(d.object);
                f.push([i, k, l, m, m.data("src")])
            }
            b(c, f, d), a(window).off("resize.flexImages" + c.data("flex-t")), a(window).on("resize.flexImages" + g, function() {
                b(c, f, d)
            }), c.data("flex-t", g)
        })
    }
}(jQuery), ! function(a) {
    a.fn.unveil = function(b) {
        function c() {
            var b = g.filter(function() {
                var b = a(this);
                if (!b.is(":hidden")) {
                    var c = e.scrollTop(),
                        d = c + e.height(),
                        g = b.offset().top,
                        h = g + b.height();
                    return h >= c - f && d + f >= g
                }
            });
            d = b.trigger("unveil"), g = g.not(d)
        }
        var d, e = a(window),
            f = b || 0,
            g = this;
        return this.one("unveil", function() {
            var a = this.getAttribute("data-lazy"),
                b = this.getAttribute("data-lazy-srcset");
            a && this.setAttribute("src", a), b && this.setAttribute("srcset", b)
        }), e.on("scroll.unveil resize.unveil lookup.unveil", c), c(), this
    }
}(window.jQuery), window.linkify = function() {
    var a = "[a-z\\d.-]+://",
        b = "(?:(?:[0-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\.){3}(?:[0-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])",
        c = "(?:(?:[^\\s!@#$%^&*()_=+[\\]{}\\\\|;:'\",.<>/?]+)\\.)+",
        d = "(?:at|biz|com|ch|co|de|edu|es|eu|fr|gov|info|it|ly|me|mobi|nl|net|org|to|uk|us|ws)",
        e = "(?:" + c + d + "|" + b + ")",
        f = "(?:[;/][^#?<>\\s]*)?",
        g = "(?:\\?[^#<>\\s]*)?(?:#[^<>\\s]*)?",
        h = "\\b" + a + "[^<>\\s]+",
        i = "\\b" + e + f + g + "(?!\\w)",
        j = "mailto:",
        k = "(?:" + j + ")?[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@" + e + g + "(?!\\w)",
        l = new RegExp("(?:" + h + "|" + i + "|" + k + ")", "ig"),
        m = new RegExp("^" + a, "i"),
        n = {
            "'": "`",
            ">": "<",
            ")": "(",
            "]": "[",
            "}": "{",
            "»": "«",
            "›": "‹"
        };
    return function(a, b) {
        b = b || {};
        for (var c, d, e, f, i, k, o, p, q, r, s, t, g = "", h = []; c = l.exec(a);)
            if (e = c[0], k = l.lastIndex, o = k - e.length, !/[\/:]/.test(a.charAt(o - 1))) {
                do p = e, t = e.substr(-1), s = n[t], s && (q = e.match(new RegExp("\\" + s + "(?!$)", "g")), r = e.match(new RegExp("\\" + t, "g")), (q ? q.length : 0) < (r ? r.length : 0) && (e = e.substr(0, e.length - 1), k--)), e = e.replace(/(?:[!?.,:;'"]|(?:&|&amp;)(?:lt|gt|quot|apos|raquo|laquo|rsaquo|lsaquo);)$/, function(a) {
                    return k -= a.length, ""
                }); while (e.length && e !== p);
                f = e, m.test(f) || (f = (f.indexOf("@") !== -1 ? f.indexOf(j) ? j : "" : f.indexOf("irc.") ? f.indexOf("ftp.") ? "http://" : "ftp://" : "irc://") + f), i != o && (h.push([a.slice(i, o)]), i = k), h.push([e, f])
            }
        for (h.push([a.substr(i)]), d = 0; d < h.length; d++) text = h[d][0], f = h[d][1], target = text.indexOf(window.location.hostname) >= 0 ? "" : 'target="_blank" ', g += f ? "<a " + target + 'href="' + f + '">' + text + "</a>" : text;
        return g || a
    }
}();
var ww, wh, pure_menu;
"ontouchstart" in window ? (human_visitor = !0, $(".pure-dropdown>a").click(function(a) {
    pure_menu = $(this).parent();
    var b = pure_menu.toggleClass("pure-menu-open").find(".pure-menu-children").css("top", pure_menu.height());
    return pure_menu.offset().left + b.outerWidth() > ww && ww > b.outerWidth() ? b.css("left", pure_menu.width() - b.outerWidth()) : b.css("left", 0), !1
}), $(document).mousedown(function(a) {
    !pure_menu || pure_menu.is(a.target) || pure_menu.has(a.target).length || ($(".pure-dropdown").removeClass("pure-menu-open"), pure_menu = !1)
})) : ($(".pure-dropdown>a").mouseenter(function(a) {
    pure_menu = $(this).parent();
    var b = pure_menu.toggleClass("pure-menu-open").find(".pure-menu-children").css("top", pure_menu.height());
    return pure_menu.offset().left + b.outerWidth() > ww && ww > b.outerWidth() ? b.css("left", pure_menu.width() - b.outerWidth()) : b.css("left", 0), !1
}), $(".pure-dropdown").mouseleave(function() {
    $(this).removeClass("pure-menu-open"), pure_menu = !1
}));
var mm = $("#mobile_menu"),
    mum = $("#mobile_user_menu");
$(".mm_inc").each(function() {
    mm.append($(this).clone()), $(this).hasClass("mm_sep") && mm.append('<li class="pure-menu-separator"></li>')
}), $(".mum_inc").each(function() {
    mum.append($(this).clone().removeClass("hide-xs hide-sm hide-md"))
});
var ajax_anim = !1,
    loadingBar = $('<div id="loadingBar"><dt></dt><dd></dd></div>');
loadingBar.appendTo("body").hide(), $.ajaxSetup({
    beforeSend: function() {
        ajax_anim && (loadingBar.css("width", 0), loadingBar.show())
    },
    error: function() {
        ajax_anim && loadingBar.css("width", "102%").addClass("error").fadeOut(500, function() {
            loadingBar.removeClass("error").hide()
        })
    },
    complete: function(a, b) {
        "error" != b && loadingBar.hide(), ajax_anim = !1
    },
    xhr: function() {
        var a = new window.XMLHttpRequest;
        try {
            a.upload.addEventListener("progress", function(a) {
                a.lengthComputable && loadingBar.css("width", 2 + 100 * a.loaded / a.total + "%")
            }, !1), a.addEventListener("progress", function(a) {
                a.lengthComputable && loadingBar.css("width", 2 + 100 * a.loaded / a.total + "%")
            }, !1)
        } catch (a) {}
        return a
    }
}), $(document).on("submit", ".ajax_form", function(e) {
    e.preventDefault();
    var form = $(this),
        t = form.data("target") ? $(form.data("target")) : form;
    ajax_anim = !0, form.ajaxSubmit({
        success: function(re) {
            "script:" == re.substring(0, 7) ? eval(re.substring(7)) : form.data("replace") ? t.replaceWith(re) : t.html(re), resized()
        }
    })
});
var resizeTimer, cboxOptions = {
    width: "95%",
    height: "95%",
    maxWidth: 640,
    maxHeight: 0,
    speed: 180,
    fadeOut: 150,
    closeButton: !1,
    scrolling: !1,
    trapFocus: !1,
    opacity: .5,
    onCleanup: function() {
        $.extend($.colorbox.settings, cboxOptions)
    }
};
$.extend($.colorbox.settings, cboxOptions), $(window).resize(function() {
    clearTimeout(resizeTimer), resizeTimer = setTimeout(resized, 200)
}), $(document).on({
    mousedown: function(a) {
        a.preventDefault();
        var b = $("#colorbox").offset(),
            c = a.pageX - b.left,
            d = a.pageY - b.top;
        $(document).on("mousemove.drag", function(a) {
            $("#colorbox").offset({
                top: a.pageY - d,
                left: a.pageX - c
            })
        })
    },
    mouseup: function() {
        $(document).unbind("mousemove.drag")
    }
}, "#colorbox h6"), $(document).on("click contextmenu", ".modal", function() {
    var that = $(this);
    return (null == that.data("confirm") || confirm(that.data("confirm"))) && $.get(that.data("href") || this.href, function(re) {
        "script:" == re.substring(0, 7) ? eval(re.substring(7)) : (that.data("w") && ($.colorbox.settings.maxWidth = that.data("w")), $.colorbox({
            html: re,
            overlayClose: null == that.data("no-overlay-close"),
            height: "auto",
            onComplete: function() {
                setTimeout(resized, 100), $("#cboxContent h6").prepend('<a onclick="$.colorbox.close();">×</a>')
            }
        }))
    }), !1
}), $(document).on("click", ".ajax", function() {
    var link = $(this);
    return (null == link.data("confirm") || confirm(link.data("confirm") || I18N_DELETE)) && (ajax_anim = !0, $.post(this.href || $(this).data("href"), function(re) {
        "script:" == re.substring(0, 7) ? eval(re.substring(7)) : (link.data("target") ? $(link.data("target")) : link).html(re), resized()
    })), !1
}), $(document).on("click dblclick", ".remove_link", function(a) {
    if ($(this).data("dblclick") && "click" == a.type) return !1;
    if (null == $(this).data("confirm") || confirm($(this).data("confirm") || I18N_DELETE)) {
        var b = $($(this).data("target"));
        b.fadeOut(200, function() {
            b.remove()
        }), $.post(this.href || $(this).data("href"))
    }
    return !1
}), $(document).on("click", ".tab_menu li", function(a) {
    return $(this).siblings().removeClass("selected").end().addClass("selected"), !1
}), $(document).on("click", ".tiny_search label", function(a) {
    var b = $(this).next(),
        c = $(this).closest("form");
    c.hasClass("active") ? b.hasClass("dirty") ? c.submit() : c.removeClass("active") : c.addClass("active")
}), $(document).on("click", "a[data-confirm]:not(.ajax, .modal, .remove_link)", function(a) {
    if (!confirm($(this).data("confirm") || I18N_DELETE)) return !1
});
var dd_box, max_zindex = 99999;
$(document).on("click", ".dd_box", function() {
    max_zindex += 1;
    var a = $(this);
    if (dd_box = a.next(), dd_box.is(":visible")) dd_box.hide(), dd_box = !1;
    else {
        $(".dd_box+div").hide();
        var c = (a.position().top, a.position().left);
        if ((a.data("left") || a.offset().left + dd_box.outerWidth() > ww && a.offset().left + a.outerWidth() - dd_box.outerWidth() > 0) && (c += a.outerWidth() - dd_box.outerWidth()), a.data("up") || a.offset().top + a.outerHeight() + dd_box.outerHeight() > $(document).scrollTop() + wh && a.offset().top - dd_box.outerHeight() > 0) var d = a.position().top - dd_box.outerHeight() - 5;
        else var d = a.position().top + a.outerHeight() + 5;
        dd_box.unbind().css({
            position: "absolute",
            top: d + "px",
            left: c + "px",
            zIndex: max_zindex
        }).show()
    }
    return !1
}), $(document).click(function(a) {
    !dd_box || dd_box.is(a.target) || dd_box.has(a.target).length || (dd_box.hide(), dd_box = !1)
});
var dl_menu;
$(function() {
        resized(), $('.media_search [name="image_type"]').change(function() {
            var a = $(".media_search"),
                b = a.find('[name="image_type"]:checked');
            $(".media_search .image_type").html(b.parent().text()), "video" != b.val() ? (a.find(".image_option").show().end().find(".video_option").hide(), a.attr("action", a.attr("action", base_url+'search'))) : (a.find(".image_option").hide().end(), a.find(".video_option").show(), a.attr("action", a.attr("action", base_url+'search')))
        }), $('.media_search [name="image_type"]').change(), setTimeout(function() {
            "ontouchstart" in window || window.location.hash || $(document).scrollTop() || $("[data-autofocus]").last().focus()
        }, 250), ~$.inArray(LANG, ["de", "en", "es", "fr", "it", "ja", "ko", "pt"]) && $('.media_search input[name="q"]').autoComplete({
            onSelect: function(a) {
                "keydown" != a.type && $(".media_search").submit()
            },
            source: function(a, b) {
                try {
                    ac_xhr.abort()
                } catch (a) {}
                ac_xhr = $.ajax({
                    url: base_url+"search/suggestion",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify({
                        my_suggest: {
                            text: a,
                            completion: {
                                field: "suggest_" + LANG,
                                size: 8
                            }
                        }
                    }),
                    success: function(a) {
                        var c = [];
                        $.each(a.my_suggest[0].options, function() {
                            c.push(this.text)
                        }), b(c)
                    }
                })
            }
        }), $(document).one("mousemove", function() {
            human_visitor = !0
        }), $(document).on("mousedown", "[data-go]", function() {
            if (human_visitor) return $(this).attr("href", "/go/?t=" + $(this).data("go")), $(this).data("go").indexOf(window.location.hostname) == -1 && "/" != $(this).data("go").charAt(0) && $(this).attr("target", "_blank"), !1
        }), $("#toTop").click(function() {
            $("body,html").animate({
                scrollTop: 0
            })
        }), $(window).scroll(function() {
            $(this).scrollTop() > 480 ? $("#toTop").addClass("show") : $("#toTop").removeClass("show")
        }), $(document).on("click", ".translate", function() {
            var a = "https://translate.google.de/#auto|" + LANG + "|" + encodeURIComponent($($(this).data("el")).text().substring(0, 999));
            return wopen(a, 800, 500, "translate")
        }), $(".linkify").each(function() {
            $(this).html(linkify($(this).html()))
        }), $(".media_list").length ? ($("#paginator_clone").html($(".paginator").clone()), $("#photo_grid").length ? ($("#photo_grid").flexImages({
            rowHeight: 200
        }), $(".flex_grid.sponsored").flexImages({
            maxRows: 1
        })) : flexVideoGrid(280, 0), $("[data-lazy]").unveil(400)) : $("#media_show.photo").length ? ($("#media_container img").click(function(a) {
            $("#support_overlay").hide();
            var b = $("#img_click_overlay");
            b.show(), b.data("cloned") || b.data("cloned", 1).find("#download_menu_clone").append($(".download_menu").clone(!0, !0)).end().find("#like_buttons_clone").append($(".like_buttons").clone(!0, !0)), $("#media_show .overlay").fadeIn(100)
        }), $("#media_show .overlay").click(function(a) {
            if ($(a.target).is("#support_overlay .pure-button, #download_menu_clone *:not(.modal)")) return void a.stopPropagation();
            if (!$(a.target).is("#like_buttons_clone *")) {
                var b = $("#support_overlay").is(":visible");
                $(this).fadeOut(200, function() {
                    b && $("#support_overlay").remove()
                })
            }
        }), $(".dl_btn, .view_btn").click(function() {
            dl_menu && dl_menu.is(":visible") && ($(this).hasClass("modal") || show_support_overlay(), setTimeout(function() {
                dl_menu && dl_menu.hide()
            }, 50))
        }), $(".flex_grid.related_photos").flexImages({
            rowHeight: 125,
            maxRows: 1
        }), $(".flex_grid.related_photos_sidebar").flexImages({
            rowHeight: 160,
            maxRows: 3
        }), $(".flex_grid.sponsored").flexImages({
            rowHeight: 160,
            maxRows: 3
        })) : $(".signup_form.new").length && $.get("/" + LANG + "/accounts/register/ets/", function(a) {
            $(".signup_form .data").each(function() {
                var b = "f-" + a + "-" + $(this).attr("name");
                $(this).attr("name", b) || $(this).prop("name", b)
            }), $(".signup_form.new").show()
        }), $("#media_show").length && (setTimeout(function() {
            $("#media_show .init").removeClass("init")
        }, 5e3), $("#media_show .init").mouseleave(function() {
            $(this).removeClass("init")
        }), $(".download_menu span").on("click contextmenu", function() {
            return dl_menu = $(this).next(), dl_menu.is(":visible") ? dl_menu.hide() : (wh - dl_menu.prev().offset().top + $(document).scrollTop() > 250 ? dl_menu.removeClass("ne").addClass("se") : dl_menu.removeClass("se").addClass("ne"), dl_menu.width(dl_menu.prev().width() + 22).show().find(".selected input").prop("checked", !0)), !1
        }), $(document).click(function(a) {
            !dl_menu || dl_menu.is(a.target) || dl_menu.has(a.target).length || (dl_menu.hide(), dl_menu = null)
        }), $(".download_menu tr").click(function() {
            $(".download_menu tr").removeClass("selected");
            var a = $(this).find("input").eq(0);
            a.prop("checked", !0), $('.download_menu [value="' + a.val() + '"]').closest("tr").addClass("selected");
            var b = a.val(),
                c = a.data("perm");
            b.indexOf("/") < 0 && (b = base_url + "/download/" + b), show_captcha && c || !auth_user && "auth" == c ? ($(".dl_btn, .view_btn").addClass("modal"), $(".dl_btn").attr("href", b + "?attachment&modal"), $(".view_btn").attr("href", b + "?modal")) : ($(".dl_btn, .view_btn").removeClass("modal"), $(".dl_btn").attr("href", b + "?attachment"), $(".view_btn").attr("href", b)), $(".dl_btn").attr("target", a.data("target") ? "_blank" : "")
        }), $(".download_menu tr:not(.no_default)").last().click());
        var a = getCookie("g_rated");
        a && $(".toggle_g_rated").prop("checked", !0), $(document).on("click", ".toggle_g_rated", function(b) {
            a = $(this).is(":checked"), setCookie("g_rated", a ? 1 : "", 365), $(".toggle_g_rated").prop("checked", a)
        })
    });